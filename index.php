<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0">
        <meta charset="utf-8">
        <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
        <style>
            html, body {
                height: 100vh;
                margin: 0;
                padding: 0;
            }

            #map {
                height: 100vh;
            }

            #buttons {
                position: absolute;
                right: 5px;
                top: 20vh;
                z-index: 1;
            }

            #buttons div {
                border-radius: 50%;
                box-shadow: 0 2px 4px rgba(0,0,0,0.2);
                width: 60px;
                height: 60px;
                overflow: hidden;
                outline: none;
                margin-bottom: 8px;
            }

            #buttons div img {
                width: 60px;
                height: 60px;
            }
        </style>
        <title>AEL - Trabajo 1</title>
    </head>
    <body>
        <div id="map"></div>
        <script>
            var map = null,
                currentMarker = null,
                lat = null,
                long = null;

            function initMap() {
                $("#buttons").hide();

                var options = {
                    enableHighAccuracy: true,
                    timeout: 5000,
                    maximumAge: 0
                };

                function success(pos) {
                    lat = pos.coords.latitude;
                    long = pos.coords.longitude;

                    map = new google.maps.Map(document.getElementById('map'), {
                        center: new google.maps.LatLng(lat, long),
                        scrollwheel: true,
                        zoom: 13
                    });

                    currentMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, long),
                        map: map,
                        title: 'Mi ubicación'
                    });

                    $("#buttons").show();
                }

                function error(err) {
                    console.warn('ERROR(' + err.code + '): ' + err.message);
                }

                navigator.geolocation.getCurrentPosition(success, error, options);
            }
        </script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoqJonEDqL5ePNAjOIlkneI417v2dJlZY&signed_in=true&callback=initMap"></script>
        <div id="buttons">
            <div id="movred"><img src="./img/movred.png" alt="movred" title="Estacionamientos para personas con movilidad reducida"></div>
            <div id="motos"><img src="./img/motos.png" alt="motos" title="Estacionamientos para motos"></div>
            <div id="ora"><img src="./img/ora.png" alt="ora" title="O. R. A."></div>
            <div id="npark"><img src="./img/npark.png" alt="npark" title="Estacionamientos para coches no regulados"></div>
            <div id="ppark"><img src="./img/ppark.png" alt="ppark" title="Parkings privados"></div>
        </div>
        <script type="application/javascript">
            var markers = [];
            markers[0] = []; // movred
            markers[1] = []; // motos
            markers[2] = []; // ora
            markers[3] = []; // npark
            markers[4] = []; // ppark

            var type = "",
                errorMsg = "",
                typeIndexMarkers = -1;

            function hideMarkers() {
                for (var i = 0; i < markers.length; i++) {
                    for (var j = 0; j < markers[i].length; j++) {
                        markers[i][j].setMap(null);
                    }
                }
            }

            $(document).ready(function () {
                $("#movred, #motos, #ora, #npark, #ppark").each(function () {
                    $(this).click(function () {
                        type = $(this).get(0).id;
                        switch (type) {
                            case "movred":
                                errorMsg = "No hay estacionamientos para movilidad reducida cerca de tu ubicación actual";
                                typeIndexMarkers = 0;
                                break;
                            case "motos":
                                errorMsg = "No hay estacionamientos para motos cerca de tu ubicación actual";
                                typeIndexMarkers = 1;
                                break;
                            case "ora":
                                errorMsg = "No hay estacionamientos de O. R. A. cerca de tu ubicación actual";
                                typeIndexMarkers = 2;
                                break;
                            case "npark":
                                errorMsg = "No hay estacionamientos no regulados para coches cerca de tu ubicación actual";
                                typeIndexMarkers = 3;
                                break;
                            case "ppark":
                                errorMsg = "No hay estacionamientos privados cerca de tu ubicación actual";
                                typeIndexMarkers = 4;
                                break;
                        }
                        hideMarkers();
                        $.ajax({
                            method: 'get',
                            url: './data-manager.php',
                            data: "type=" + type + "&lat=" + lat + "&long=" + long,
                            dataType: 'json'
                        }).done(function (res) {
                            if (res.length == 0) {
                                alert(errorMsg);
                            } else {
                                var location = null,
                                    image = {
                                        url: "./img/" + type + "_marker.png",
                                        scaledSize : new google.maps.Size(35, 35)
                                    };
                                for (var i = 0; i < res.length; i++) {
                                    location = new google.maps.LatLng(res[i]['lat'], res[i]['long']);
                                    markers[typeIndexMarkers][i] = new google.maps.Marker({
                                        position: location,
                                        map: map,
                                        icon: image
                                    });
                                }
                            }
                        }).fail(function (f) {
                            alert("Error: " + String(f));
                        });
                    });
                });
            });
        </script>
    </body>
</html>